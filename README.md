Updated NPC sheet that looks a bit more like the statblock from the book.

Features:
- Supports many additional fields that were in some stat blocks. Examples include:
-- Detail text modifiers for AC, HP, Saves, senses
-- Support for languages
-- List of equipment held by NPC's (drag/droppable)
-- Multiple speeds (i.e. flying, burrowing)
-- Support for multiple damage types and attack effects on a Melee attack.
- Follow's book statblock layout to splits actions based on whether they are interaction, defensive, or offensive so it's easier to look at the actions you care about during play.
- Toggle-able edit mode so that editing an NPC doesn't limit how pretty/usable the sheet is in regular play. (Yes, edit mode is ugly/hacked on the base sheet)

![Example Image](preview.gif)

To do:
- Add Recall Knowledge display
- Fix NPC traits to not be static
- Add a way to edit NPC traits, languages
- Add a way to remove equipment
- Add a way to change an action from interaction<-->offensive<-->defensive without having to import with beastiary pack.
- Add a way to define more damageRolls and attackEffects in the sheet